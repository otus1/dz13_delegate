﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dz13_delegate.Extension
{
    public static class Extension
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            var max = float.MinValue;
            T maxEl = null;
            foreach (var el in e)
            {
                var tmpMax = getParametr(el);
                if (tmpMax > max) 
                {
                    max = tmpMax;
                    maxEl = el;
                }
            }
            return maxEl;
        }
    }
}
