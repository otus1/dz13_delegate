﻿using dz13_delegate.Extension;
using dz13_delegate.Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace dz13_delegate
{
    class Program
    {
        static void Main(string[] args)
        {
            //Задание №1
            var childList = new List<Child>()
            {
                new Child("Вася", 10),
                new Child("Петя", 12),
                new Child("Коля", 15),
                new Child("Аня", 10),
                new Child("Света", 13)
            };
            var el = childList.GetMax(x => x.Age);
            Console.WriteLine($"Max element: {el} \n\n");

            //Здание №2
            Console.WriteLine($"Список файлов:");
            var findFiles = new FindFiles(Directory.GetCurrentDirectory());
            findFiles.FileFound += FileFoundEvent;
            findFiles.Find();

            Console.ReadKey();
        }
        private static void  FileFoundEvent(object sender, FileArgs e)
        {
            Console.WriteLine($"Файл: {e.Name}");
            Console.WriteLine("Ищем дальше(Y/N)? ");
            var res = Console.ReadLine();
            e.isExit = res.ToLower() == "n";
            if (e.isExit)
                Console.WriteLine("Прервона из делегата");
        }
    }
}
