﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dz13_delegate.Model
{
    public class Child
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Child(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public override string ToString()
        {
            return $"{Name} (Age: {Age})";
        }
    }
}
