﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace dz13_delegate.Model
{
    public class FindFiles
    {
        public string Dir { get; set; }
        public event EventHandler<FileArgs> FileFound;

        public FindFiles(string dir)
        {
            Dir = dir;
        }
        public void Find()
        {
            foreach (var file in Directory.GetFiles(Dir))
            {
                var args = new FileArgs { Name = file };
                FileFound?.Invoke(this, args);
                if (args.isExit)
                    break;
            }
        }
    }
}
